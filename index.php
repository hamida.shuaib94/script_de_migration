<?php

require_once 'config.php';

spl_autoload_register(function ($class) {
    require __DIR__ . DIRECTORY_SEPARATOR . strtolower(str_replace('\\', DIRECTORY_SEPARATOR, $class)) . '.php';
});

require_once __DIR__ . '/views/header.html';
require_once __DIR__ . '/views/menu.php';
require_once __DIR__ . '/views/body.php';
require_once __DIR__ . '/views/footer.html';

