<?php
if (!is_object($data)) {
    return false;
}
?>

<table>
    <thead>
        <tr>
            <th>nom</th>
            <th>code</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data->data as $course) {
            echo '<tr><td>' . $course->name . '</td><td>' . $course->code . '</td></tr>';
        }
        ?>
    </tbody>
</table>
