<?php
if (!is_object($data)) {
    return false;
}
?>

<table>
    <thead>
        <tr>
            <th>id</th>
            <th>login</th>
            <th>email</th>
            <th>lastlogin</th>
            <th>created</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data->data as $user) {
            echo '<tr><td>' . $user->id . '</td><td>' . $user->login . '</td><td>' . $user->email . '</td><td>' . $user->lastlogin . '</td><td>' . $user->created . '</td></tr>';
        }
        ?>
    </tbody>
</table>
