<?php

namespace app\models;

use app\helpers\database;

class user extends Model
{
    public function getAll(string $user): array
    {
        $courses = [];
        $connect = database::connect();
        $request = $connect->query("SELECT * FROM $user");
        while ($course = $request->fetchObject()) {
            $courses[] = $course;
        }
        return $courses;
    }
}
