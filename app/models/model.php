<?php 
namespace app\models;
use app\helpers\database;


abstract class Model {
    protected $connect;
    public function getAll(string $table): array
    {
        $courses = [];
        $connect = database::connect();
        $request = $connect->query("SELECT * FROM $table");
        while ($course = $request->fetchObject()) {
            $courses[] = $course;
        }
        return $courses;
    }

} 