<?php

namespace app\models;

use app\helpers\database;

class course extends Model
{
    public function getAll(string $course): array
    {
        $courses = [];
        $connect = database::connect();
        $request = $connect->query("SELECT * FROM $course");
        while ($course = $request->fetchObject()) {
            $courses[] = $course;
        }
        return $courses;
    }
}
